# BetPawaClient


require jmeter-3.1

1.  .jar file

    a. mvn clean package    
    b. copy jar file into jmeter_home/lib/ext
    
2.  Jmeter 3.1
    
    a. in bin directory: sh jmeter.sh     
    b. select test.jmx from jmeter-tests     
    c. setup tests     
    d. run tests from GUI from console: 
    
    sh jmeter.sh -n -t test.jmx -l testresults.jtl

 enjoy   