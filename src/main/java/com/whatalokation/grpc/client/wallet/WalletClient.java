package com.whatalokation.grpc.client.wallet;

import com.whatalokation.grpc.client.greeter.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WalletClient {

    private static final Logger logger = Logger.getLogger("Whatalokation");

    private final ManagedChannel channel;

    //deposit
    private final DepositProtoGrpc.DepositProtoBlockingStub depositBlockingStub;
    private final DepositProtoGrpc.DepositProtoStub depositAsyncsyncStub;

    //balance
    private final BalanceProtoGrpc.BalanceProtoBlockingStub balanceBlockingStub;
    private final BalanceProtoGrpc.BalanceProtoStub balanceAsyncsyncStub;

    //withdrow
    private final WithdrawProtoGrpc.WithdrawProtoBlockingStub withdrowBlockingStub;
    private final WithdrawProtoGrpc.WithdrawProtoStub withdrowAsyncsyncStub;

    /**
     * Construct client connecting to Greeter server at {@code host:port}.
     */
    public WalletClient(String host, int port) {

        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example
                // we disable TLS to avoid
                // needing certificates.
                .usePlaintext(true));
        logger.setLevel(Level.WARNING);
    }

    WalletClient(ManagedChannelBuilder<?> channelBuilder) {
        channel = channelBuilder.build();

        //deposit
        depositBlockingStub = DepositProtoGrpc.newBlockingStub(channel);
        depositAsyncsyncStub = DepositProtoGrpc.newStub(channel);

        //balance
        balanceBlockingStub = BalanceProtoGrpc.newBlockingStub(channel);
        balanceAsyncsyncStub = BalanceProtoGrpc.newStub(channel);

        //withdrow

        withdrowBlockingStub = WithdrawProtoGrpc.newBlockingStub(channel);
        withdrowAsyncsyncStub = WithdrawProtoGrpc.newStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public String getBalance(String user) {
       // logger.info("running greeter check health ");
        BalanceReq request = BalanceReq.newBuilder().setUserId(user).build();
        BalanceRes response = null;
        try {
            response = balanceBlockingStub.balance(request);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            throw e;
        }
        System.out.println("Balance: " + Arrays.toString(response.getBalanceList().toArray()));
        return response.getBalanceList().toString();
    }

    public String deposit(String user,double amount, String cur) {
       // logger.info("running greeter check health ");
        DepositReq request = DepositReq.newBuilder()
                .setUserId(user)
                .setAmount(amount)
                .setCur(cur)
                .build();
        DepositRes response = null;
        try {
            response = depositBlockingStub.deposit(request);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            throw e;
        }
       // System.out.println("Deposit result: " + response);
        return response.toString();
    }

    public String withdraw(String user, double amount,String cur)
    {
       // logger.info("running greeter check health ");
        WithdrawReq request = WithdrawReq.newBuilder()
                .setUserId(user)
                .setAmount(amount)
                .setCur(cur)
                .build();

        WithdrawRes response = null;
        try {
            response = withdrowBlockingStub.withdraw(request);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            throw e;
        }
        //System.out.println("Withdrow result: " + response);
        return response.toString();
    }

    public void roundA(String user)
    {
        deposit(user,100,"EUR");
        withdraw(user,200,"EUR");
        deposit(user,100,"EUR");
        getBalance(user);
        withdraw(user, 100, "EUR");
        getBalance(user);
        withdraw(user, 100, "EUR");
    }

    public void roundB(String user)
    {
        withdraw(user,100,"GBP");
        deposit(user,300,"GBP");
        withdraw(user,100,"GBP");
        withdraw(user,100,"GBP");
        withdraw(user,100,"GBP");
        getBalance(user);
    }

    public void roundC(String user)
    {
        getBalance(user);
        deposit(user,100,"USD");
        deposit(user,100,"USD");
        withdraw(user,100,"USD");
        deposit(user,100,"USD");
        getBalance(user);
        withdraw(user,200,"USD");
        getBalance(user);

    }


    public void randomRound(String user)
    {
        Random r= new Random();
        int randimInt=r.nextInt(3);
        switch (randimInt){
            case 0: roundA(user); break;
            case 1: roundB(user); break;
            case 2: roundC(user); break;
            default:
                System.out.println("WTF");
        }
    }
}
