package com.whatalokation.jmeter.greeter;

import com.whatalokation.grpc.client.wallet.WalletClient;
import io.grpc.StatusRuntimeException;

import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

import com.whatalokation.grpc.client.greeter.GreeterClient;

public class GreeterHealthCheckSampler extends AbstractJavaSamplerClient{
	
	WalletClient wClient = null;
	String user;
	int roundCount;
	
	@Override
    public void setupTest(JavaSamplerContext context){
		String environment = context.getParameter("environment");
		String port = context.getParameter("port");
		user = context.getParameter("user");
		roundCount=Integer.parseInt(context.getParameter("roundCount"));
		this.wClient = new WalletClient("localhost", 6565);
		
		super.setupTest(context);
    }
	
	@Override
	public Arguments getDefaultParameters() {
	    Arguments defaultParameters = new Arguments();
	    defaultParameters.addArgument("environment", "localhost");
	    defaultParameters.addArgument("port", "6565");
		defaultParameters.addArgument("user", "1");
		defaultParameters.addArgument("roundCount", "25");

	    return defaultParameters;
	}
	
	public SampleResult runTest(JavaSamplerContext context) {
		
	    SampleResult result = new SampleResult();
	    boolean success = true;
	    String response = "";
	    result.sampleStart();
	    
	    try {
			for (int i = 0; i < roundCount; i++) {
				this.wClient.randomRound(user);
			}

	    	result.sampleEnd();
	    	result.setSuccessful(success);
	    	result.setResponseData("OK".getBytes());
	    	result.setResponseMessage( "Successfully performed random round");
            result.setResponseCodeOK(); // 200 code
	    } 
	    catch (StatusRuntimeException e){
	    	result.sampleEnd(); // stop stopwatch

            result.setResponseMessage("Exception: " + e);
	    	success = true;
	    	result.setSuccessful(success);
	    	// get stack trace as a String to return as document data
            java.io.StringWriter stringWriter = new java.io.StringWriter();
            e.printStackTrace( new java.io.PrintWriter(stringWriter));
            result.setResponseData(stringWriter.toString().getBytes());
            result.setDataType( org.apache.jmeter.samplers.SampleResult.TEXT );
            result.setResponseCode("500");
	    }
	    catch (Exception ex){
			result.sampleEnd(); // stop stopwatch
			result.setSuccessful(false);
			result.setResponseMessage("Exception: " + ex);
			success = false;
			result.setSuccessful(success);
			// get stack trace as a String to return as document data
			java.io.StringWriter stringWriter = new java.io.StringWriter();
			ex.printStackTrace( new java.io.PrintWriter(stringWriter));
			result.setResponseData(stringWriter.toString().getBytes());
			result.setDataType( org.apache.jmeter.samplers.SampleResult.TEXT );
			result.setResponseCode("500");
		}
	    	    
	    return result;
	}
	
	@Override
    public void teardownTest(JavaSamplerContext context){
		try {
			wClient.shutdown();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		super.teardownTest(context);
    }
	
}
